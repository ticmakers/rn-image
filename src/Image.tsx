import * as React from 'react'
import { View, ActivityIndicator, Image as RN_Image, ImageProps as RN_ImageProps, StyleSheet } from 'react-native'
import { Image as RNE_Image, ImageProps as RNE_ImageProps } from 'react-native-elements'

import {
  IImageProps,
  IImageState,
  TypeImageOnErrorArgs,
  TypeImageOnLayoutArgs,
  TypeImageOnLoadArgs,
  TypeImageOnProgressArgs,
} from './../index.d'

import styles from './styles'

/**
 * Class to define the component Image
 * @class Image
 * @extends {React.Component<IImageProps, IImageState>}
 * @noInheritDoc
 */
export default class Image extends React.Component<IImageProps, IImageState> {
  constructor(props: IImageProps) {
    super(props)

    this.state = {
      loading: false,
    }
  }

  /**
   * Method that renders the component
   * @returns {JSX.Element}
   */
  public render(): JSX.Element {
    const { loading } = this.state
    const { accessibilityLabel, accessible, blurRadius, capInsets, containerStyle, defaultSource, fadeDuration, ImageComponent, loadingIndicatorSource, PlaceholderContent, placeholderStyle, progressiveRenderingEnabled, resizeMethod, resizeMode, source, style, testID, ui } = this._processProps()

    const styleLoading = { opacity: loading ? 0 : 1 }

    const rnProps: RN_ImageProps = {
      accessibilityLabel,
      accessible,
      blurRadius,
      capInsets,
      defaultSource,
      fadeDuration,
      loadingIndicatorSource,
      progressiveRenderingEnabled,
      resizeMethod,
      resizeMode,
      source,
      testID,
      // tslint:disable-next-line: object-literal-sort-keys
      onError: this._onError.bind(this),
      onLayout: this._onLayout.bind(this),
      onLoad: this._onLoad.bind(this),
      onLoadEnd: this._onLoadEnd.bind(this),
      onLoadStart: this._onLoadStart.bind(this),
      onPartialLoad: this._onPartialLoad.bind(this),
      onProgress: this._onProgress.bind(this),
      style: StyleSheet.flatten([style, styleLoading]),
    }

    const rneProps: RNE_ImageProps = {
      ...rnProps,
      ImageComponent,
      containerStyle,
      placeholderStyle,
      // tslint:disable-next-line: object-literal-sort-keys
      PlaceholderContent: PlaceholderContent || <ActivityIndicator />,
    }

    if (ui === 'react-native-elements') {
      return <RNE_Image { ...rneProps } />
    }

    return (
      <View style={ styles.container }>
        { loading && <ActivityIndicator size="large" style={ styles.loading } /> }
        <RN_Image { ...rnProps } />
      </View>
    )
  }

  /**
   * Method that fire when the image has error
   * @private
   * @param {TypeImageOnErrorArgs} err
   * @returns {void}
   */
  private _onError(err: TypeImageOnErrorArgs): void {
    const { onError } = this._processProps()
    console.error(err)

    if (onError) {
      return onError(err)
    }
  }

  /**
   * Method that fire when the image is on layout
   * @private
   * @param {TypeImageOnLayoutArgs} layout
   * @returns {void}
   */
  private _onLayout(layout: TypeImageOnLayoutArgs): void {
    const { onLayout } = this._processProps()

    if (onLayout) {
      return onLayout(layout)
    }
  }

  /**
   * Method that fire when the image is loading
   * @private
   * @param {TypeImageOnLoadArgs} source
   * @returns {void}
   */
  private _onLoad(source: TypeImageOnLoadArgs): void {
    const { onLoad } = this._processProps()
    this.setState({ loading: false })

    if (onLoad) {
      return onLoad(source)
    }
  }

  /**
   * Method that fire when the image is loaded
   * @private
   * @returns {void}
   */
  private _onLoadEnd(): void {
    const { onLoadEnd } = this._processProps()

    if (onLoadEnd) {
      return onLoadEnd()
    }
  }

  /**
   * Method that fire when the image is start load
   * @private
   * @returns {void}
   */
  private _onLoadStart(): void {
    const { onLoadStart } = this._processProps()
    this.setState({ loading: true })

    if (onLoadStart) {
      return onLoadStart()
    }
  }

  /**
   * Method that fire when the image is partial load
   * @private
   * @returns {void}
   * @platform iOS
   */
  private _onPartialLoad(): void {
    const { onPartialLoad } = this._processProps()

    if (onPartialLoad) {
      return onPartialLoad()
    }
  }

  /**
   * Method that fire when the image is on progress
   * @private
   * @returns {void}
   * @platform iOS
   */
  private _onProgress(event: TypeImageOnProgressArgs): void {
    const { onProgress } = this._processProps()

    if (onProgress) {
      return onProgress(event)
    }
  }

  /**
   * Method to process the props of the component
   * @private
   * @returns {IImageProps}
   */
  private _processProps(): IImageProps {
    const { accessibilityLabel, accessible, blurRadius, capInsets, containerStyle, defaultSource, fadeDuration, ImageComponent, loadingIndicatorSource, onError, onLayout, onLoad, onLoadEnd, onLoadStart, onPartialLoad, onProgress, progressiveRenderingEnabled, resizeMethod, resizeMode, PlaceholderContent, placeholderStyle, source, style, testID, ui } = this.props

    return {
      ImageComponent: ImageComponent || undefined,
      PlaceholderContent: PlaceholderContent || undefined,
      accessibilityLabel: accessibilityLabel || undefined,
      accessible: accessible || false,
      blurRadius: blurRadius || undefined,
      capInsets: capInsets || undefined,
      containerStyle: containerStyle || undefined,
      defaultSource: defaultSource || undefined,
      fadeDuration: fadeDuration || 300,
      loadingIndicatorSource: loadingIndicatorSource || undefined,
      onError: onError || undefined,
      onLayout: onLayout || undefined,
      onLoad: onLoad || undefined,
      onLoadEnd: onLoadEnd || undefined,
      onLoadStart: onLoadStart || undefined,
      onPartialLoad: onPartialLoad || undefined,
      onProgress: onProgress || undefined,
      placeholderStyle: placeholderStyle || undefined,
      progressiveRenderingEnabled: progressiveRenderingEnabled || false,
      resizeMethod: resizeMethod || undefined,
      resizeMode: resizeMode || undefined,
      source: source || { uri: undefined },
      style: style || undefined,
      testID: testID || undefined,
      ui: ui || 'react-native',
    }
  }
}
