import * as React from 'react'
import {
  ImageLoadEventData,
  ImageErrorEventData,
  ImageProgressEventDataIOS,
  ImageSourcePropType,
  Insets,
  LayoutChangeEvent,
  NativeSyntheticEvent,
  ImageURISource,
} from 'react-native'

/**
 * `auto`: Use heuristics to pick between resize and scale.
 *
 * `resize`: A software operation which changes the encoded image in memory before it gets decoded. This should be used instead of scale when the image is much larger than the view.
 *
 * `scale`: The image gets drawn downscaled or upscaled. Compared to resize, scale is faster (usually hardware accelerated) and produces higher quality images. This should be used if the image is smaller than the view. It should also be used if the image is slightly bigger than the view.
 *
 * @platform Android
 */
export type TypeImageResizeMethod = 'auto' | 'resize' | 'scale'

/**
 * `center`: Center the image in the view along both dimensions. If the image is larger than the view, scale it down uniformly so that it is contained in the view.
 *
 * `contain`: Scale the image uniformly (maintain the image's aspect ratio) so that both dimensions (width and height) of the image will be equal to or less than the corresponding dimension of the view (minus padding).
 *
 * `cover`: Scale the image uniformly (maintain the image's aspect ratio) so that both dimensions (width and height) of the image will be equal to or larger than the corresponding dimension of the view (minus padding).
 *
 * `repeat`: Repeat the image to cover the frame of the view. The image will keep its size and aspect ratio, unless it is larger than the view, in which case it will be scaled down uniformly so that it is contained in the view.
 *
 * `stretch`: Scale width and height independently, This may change the aspect ratio of the src.
 */
export type TypeImageResizeMode = 'center' | 'contain' | 'cover' | 'repeat' | 'stretch'

/**
 * Type to define the prop source of the Image component
 */
export type TypeImageSource = ImageSourcePropType

/**
 * Type to define the args to method onError of the Image component
 */
export type TypeImageOnErrorArgs = NativeSyntheticEvent<ImageErrorEventData>

/**
 * Type to define the args to method onLayout of the Image component
 */
export type TypeImageOnLayoutArgs = LayoutChangeEvent

/**
 * Type to define the args to method onLoad of the Image component
 */
export type TypeImageOnLoadArgs = NativeSyntheticEvent<ImageLoadEventData>

/**
 * Type to define the args to method onProgress of the Image component
 */
export type TypeImageOnProgressArgs = NativeSyntheticEvent<ImageProgressEventDataIOS>

/**
 * Interface to define the styles attributes of the Image component
 * @interface IImageStyle
 */
export interface IImageStyle {
  /**
   * Apply visibility to backface
   */
  backfaceVisibility?: 'visible' | 'hidden'

  /**
   * Apply a background color `(hex,rgb,etc...)`
   */
  backgroundColor?: string

  /**
   * Set true for apply a bordered style
   */
  bordered?: boolean

  /**
   * Apply a radius to the border bottom left
   */
  borderBottomLeftRadius?: number

  /**
   * Apply a radius to the border bottom right
   */
  borderBottomRightRadius?: number

  /**
   * Apply a border color `(hex,rgb,etc...)`
   */
  borderColor?: string

  /**
   * Apply a border radius to all corners
   */
  borderRadius?: number | string

  /**
   * Apply a radius to the border top left
   */
  borderTopLeftRadius?: number

  /**
   * Apply a radius to the border top right
   */
  borderTopRightRadius?: number

  /**
   * Apply the size of the border
   */
  borderWidth?: number

  /**
   * Apply opacity `(min:0, max:1)`
   */
  opacity?: number

  /**
   * Defines whether it shows or hides the content that comes out of the container
   */
  overflow?: 'visible' | 'hidden'

  /**
   * When the image has rounded corners, specifying an overlayColor will cause the remaining space in the corners to be filled with a solid color
   */
  overlayColor?: string

  /**
   * Determines how to resize the image when the frame doesn't match the raw image dimensions
   */
  resizeMode?: TypeImageResizeMode

  /**
   * Changes the color of all the non-transparent pixels to the tintColor
   */
  tintColor?: string
}

/**
 * Interface to define the props of the Image component
 * @interface IImageProps
 */
export interface IImageProps {
  /**
   * The text that's read by the screen reader when the user interacts with the image
   * @platform iOS
   */
  accessibilityLabel?: any

  /**
   * When true, indicates the image is an accessibility element
   * @platform iOS
   */
  accessible?: boolean

  /**
   * The blur radius of the blur filter added to the image
   */
  blurRadius?: number

  /**
   * When the image is resized, the corners of the size specified by `capInsets` will stay a fixed size, but the center content and borders of the image will be stretched
   * @platform iOS
   */
  capInsets?: Insets

  /**
   * Replace the style of the container of the image
   */
  containerStyle?: object

  /**
   * A static image to display while loading the image source
   *
   * If passing a number: Opaque type returned by something like `require('./image.jpg')`
   */
  defaultSource?: { uri: string } | number

  /**
   * Fade duration of the image in milliseconds
   * @default 300
   * @platform Android
   */
  fadeDuration?: number

  /**
   * Specify a different component as the Image component
   */
  ImageComponent?: React.ComponentClass<any, any>

  /**
   * Similarly to source, this property represents the resource used to render the loading indicator for the image, displayed until image is ready to be displayed, typically after when it got downloaded from network.
   */
  loadingIndicatorSource?: ImageURISource | undefined


  /**
   * Invoked on load error with `{nativeEvent: {error}}`
   */
  onError?: (err: TypeImageOnErrorArgs) => any

  /**
   * Invoked on mount and layout changes with `{nativeEvent: {layout: {x, y, width, height}}}`
   */
  onLayout?: (layout: TypeImageOnLayoutArgs) => any

  /**
   * Invoked when load completes successfully `{source: { url, height, width }}`
   */
  onLoad?: (source: TypeImageOnLoadArgs) => any

  /**
   * Invoked when load either succeeds or fails
   */
  onLoadEnd?: () => any

  /**
   * Invoked on load start
   */
  onLoadStart?: () => any

  /**
   * Invoked when a partial load of the image is complete. The definition of what constitutes a "partial load" is loader specific though this is meant for progressive JPEG loads
   * @platform iOS
   */
  onPartialLoad?: () => any

  /**
   * Invoked on download progress with `{nativeEvent: {loaded, total}}`
   * @platform iOS
   */
  onProgress?: (event: TypeImageOnProgressArgs) => any

  /**
   * A component to show while the image is loading
   */
  PlaceholderContent?: JSX.Element

  /**
   * Replace the style of the container placeholder
   */
  placeholderStyle?: object

  /**
   * When true, enables progressive jpeg streaming
   * @see https://frescolib.org/docs/progressive-jpegs.html
   * @platform Android
   */
  progressiveRenderingEnabled?: boolean

  /**
   * The mechanism that should be used to resize the image when the image's dimensions differ from the image view's dimensions
   * @default auto
   */
  resizeMethod?: TypeImageResizeMethod

  /**
   * Determines how to resize the image when the frame doesn't match the raw image dimensions
   */
  resizeMode?: TypeImageResizeMode

  /**
   * Set true for apply a rounded style
   */
  rounded?: boolean

  /**
   * The image source (either a remote URL or a local file resource)
   */
  source: TypeImageSource

  /**
   * Replace the style of the image component
   */
  style?: object

  /**
   * A unique identifier for this element to be used in UI Automation testing scripts
   */
  testID?: string

  /**
   * Define the UI component to use
   * @default react-native
   */
  ui?: 'react-native' | 'react-native-elements'
}

/**
 * Interface to define the state of the Image component
 * @interface IImageState
 */
export interface IImageState {
  /**
   * Toggle to show or hide loading
   * @default false
   */
  loading: boolean
}

/**
 * Declaration for Image component
 * @class Image
 * @extends {React.Component<IImageProps, IImageState>}
 */
declare class Image extends React.Component<IImageProps, IImageState> {
}

/**
 * Declaration for Image module
 */
declare module '@ticmakers-react-native/image'

/**
 * Export default
 */
export default Image
