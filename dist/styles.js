"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_native_1 = require("react-native");
const styles = react_native_1.StyleSheet.create({
    container: {
        alignItems: 'center',
        display: 'flex',
        justifyContent: 'center',
        position: 'relative',
    },
    loading: {
        position: 'absolute',
    },
});
exports.default = styles;
//# sourceMappingURL=styles.js.map