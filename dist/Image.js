"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_native_1 = require("react-native");
const react_native_elements_1 = require("react-native-elements");
const styles_1 = require("./styles");
class Image extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
        };
    }
    render() {
        const { loading } = this.state;
        const { accessibilityLabel, accessible, blurRadius, capInsets, containerStyle, defaultSource, fadeDuration, ImageComponent, loadingIndicatorSource, PlaceholderContent, placeholderStyle, progressiveRenderingEnabled, resizeMethod, resizeMode, source, style, testID, ui } = this._processProps();
        const styleLoading = { opacity: loading ? 0 : 1 };
        const rnProps = {
            accessibilityLabel,
            accessible,
            blurRadius,
            capInsets,
            defaultSource,
            fadeDuration,
            progressiveRenderingEnabled,
            resizeMethod,
            resizeMode,
            source,
            testID,
            loadingIndicatorSource: loadingIndicatorSource || require('./../assets/loading.gif'),
            onError: this._onError.bind(this),
            onLayout: this._onLayout.bind(this),
            onLoad: this._onLoad.bind(this),
            onLoadEnd: this._onLoadEnd.bind(this),
            onLoadStart: this._onLoadStart.bind(this),
            onPartialLoad: this._onPartialLoad.bind(this),
            onProgress: this._onProgress.bind(this),
            style: react_native_1.StyleSheet.flatten([style, styleLoading]),
        };
        const rneProps = Object.assign(Object.assign({}, rnProps), { ImageComponent,
            containerStyle,
            placeholderStyle, PlaceholderContent: PlaceholderContent || React.createElement(react_native_1.ActivityIndicator, null) });
        if (ui === 'react-native-elements') {
            return React.createElement(react_native_elements_1.Image, Object.assign({}, rneProps));
        }
        return (React.createElement(react_native_1.View, { style: styles_1.default.container },
            loading && React.createElement(react_native_1.ActivityIndicator, { size: "large", style: styles_1.default.loading }),
            React.createElement(react_native_1.Image, Object.assign({}, rnProps))));
    }
    _onError(err) {
        const { onError } = this._processProps();
        console.error(err);
        if (onError) {
            return onError(err);
        }
    }
    _onLayout(layout) {
        const { onLayout } = this._processProps();
        if (onLayout) {
            return onLayout(layout);
        }
    }
    _onLoad(source) {
        const { onLoad } = this._processProps();
        this.setState({ loading: false });
        if (onLoad) {
            return onLoad(source);
        }
    }
    _onLoadEnd() {
        const { onLoadEnd } = this._processProps();
        if (onLoadEnd) {
            return onLoadEnd();
        }
    }
    _onLoadStart() {
        const { onLoadStart } = this._processProps();
        this.setState({ loading: true });
        if (onLoadStart) {
            return onLoadStart();
        }
    }
    _onPartialLoad() {
        const { onPartialLoad } = this._processProps();
        if (onPartialLoad) {
            return onPartialLoad();
        }
    }
    _onProgress(event) {
        const { onProgress } = this._processProps();
        if (onProgress) {
            return onProgress(event);
        }
    }
    _processProps() {
        const { accessibilityLabel, accessible, blurRadius, capInsets, containerStyle, defaultSource, fadeDuration, ImageComponent, loadingIndicatorSource, onError, onLayout, onLoad, onLoadEnd, onLoadStart, onPartialLoad, onProgress, progressiveRenderingEnabled, resizeMethod, resizeMode, PlaceholderContent, placeholderStyle, source, style, testID, ui } = this.props;
        return {
            ImageComponent: ImageComponent || undefined,
            PlaceholderContent: PlaceholderContent || undefined,
            accessibilityLabel: accessibilityLabel || undefined,
            accessible: accessible || false,
            blurRadius: blurRadius || undefined,
            capInsets: capInsets || undefined,
            containerStyle: containerStyle || undefined,
            defaultSource: defaultSource || undefined,
            fadeDuration: fadeDuration || 300,
            loadingIndicatorSource: loadingIndicatorSource || undefined,
            onError: onError || undefined,
            onLayout: onLayout || undefined,
            onLoad: onLoad || undefined,
            onLoadEnd: onLoadEnd || undefined,
            onLoadStart: onLoadStart || undefined,
            onPartialLoad: onPartialLoad || undefined,
            onProgress: onProgress || undefined,
            placeholderStyle: placeholderStyle || undefined,
            progressiveRenderingEnabled: progressiveRenderingEnabled || false,
            resizeMethod: resizeMethod || undefined,
            resizeMode: resizeMode || undefined,
            source: source || { uri: undefined },
            style: style || undefined,
            testID: testID || undefined,
            ui: ui || 'react-native',
        };
    }
}
exports.default = Image;
//# sourceMappingURL=Image.js.map