[@ticmakers-react-native/image](../README.md) > ["styles"](../modules/_styles_.md)

# External module: "styles"

## Index

### Variables

* [styles](_styles_.md#styles)

---

## Variables

<a id="styles"></a>

### `<Const>` styles

**● styles**: *`object`* =  StyleSheet.create({
  container: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'center',
    position: 'relative',
  },

  loading: {
    position: 'absolute',
  },
})

*Defined in styles.ts:3*

#### Type declaration

 container: `object`

 alignItems: "center"

 display: "flex"

 justifyContent: "center"

 position: "relative"

 loading: `object`

 position: "absolute"

___

