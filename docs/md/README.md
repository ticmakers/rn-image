
TIC Makers - React Native Image
===============================

React native component for image.

Powered by [TIC Makers](https://ticmakers.com)

Demo
----

Image Expo's snack

Install
-------

Install `@ticmakers-react-native/image` package and save into `package.json`:

NPM

```shell
$ npm install @ticmakers-react-native/image --save
```

Yarn

```shell
$ yarn add @ticmakers-react-native/image
```

How to use?
-----------

```javascript
import React from 'react'
import Image from '@ticmakers-react-native/image'

export default class App extends React.Component {

  render() {
    return (
      <Image source={{ uri: 'http://example.com/assets/pic.jpg' }} />
      // OR
      <Image source={ require('./assets/profile.png.') } />
    )
  }
}
```

Properties
----------

Name

Type

Default Value

Definition

source

\-

\-

\-

Todo
----

*   Test on iOS
*   Improve and add new features
*   Improve readme
*   Create tests

Version 1.0.1 ([Changelog](https://bitbucket.org/ticmakers/rn-image/src/master/CHANGELOG.md))
---------------------------------------------------------------------------------------------

## Index

### External modules

* ["Image"](modules/_image_.md)
* ["index"](modules/_index_.md)
* ["styles"](modules/_styles_.md)

---

