# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Released

## [1.0.2] - 2019-09-25

### Fixed

- Fixed error assets

## [1.0.1] - 2019-09-24

### Added

- Added spinner loader when image is loading

### Fixed

- Fixed dependencies version
- Fixed image not showing property

## [1.0.0] - 2019-04-01

### Release

### Fixed

- Fix example README

# Unreleased

## [1.0.0-beta.1] - 2019-04-01

### Fixed

- Fix react import

## [1.0.0-beta.0] - 2019-04-01

### Added

- Upload and publish first version

[1.0.2]: https://bitbucket.org/ticmakers/rn-image/src/v1.0.2/
[1.0.1]: https://bitbucket.org/ticmakers/rn-image/src/v1.0.1/
[1.0.0]: https://bitbucket.org/ticmakers/rn-image/src/v1.0.0/
[1.0.0-beta.1]: #
[1.0.0-beta.0]: #
